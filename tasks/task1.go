package main

func main() {
	for i := 1; i <= 100; i++ {
		FizzBuzz(i)
	}
}

func FizzBuzz(num int) {
	var result string
	if num % 3 == 0 && num % 5 == 0 {
		result = "FizzBuzz"
	}else if num % 3 == 0{
		result = "Fizz"
	}else if num % 5 == 0 {
		result = "Buzz"
	}else {
		result = strconv.Itoa(num)
	}
	return result
}
