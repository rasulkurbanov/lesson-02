package main

import (
	"fmt"
	"time"
)

func main() {
	dobStr := "21.03.1999" // Replace this date with your birthday
	givenDate, err := time.Parse("02.01.2006", dobStr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s is %s", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string) {
	day := date.Weekday().String()
	switch day {
	case "Sunday":
		weekday = "Yakshanba"
	case "Monday":
		weekday = "Dushanba"
	case "Tuesday":
		weekday = "Seshanba"
	case "Wednesday":
		weekday = "Chorshanba"
	case "Thursday":
		weekday = "Payshanba"
	case "Friday":
		weekday = "Juma"
	case "Saturday":
		weekday = "Shanba"
	}
	return weekday
}
